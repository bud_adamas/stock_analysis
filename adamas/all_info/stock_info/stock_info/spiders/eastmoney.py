# -*- coding: utf-8 -*-
import scrapy

from stock_info.items import StockInfoItem

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

class EastmoneySpider(CrawlSpider):
    name = "eastmoney"
    allowed_domains = ["http://quote.eastmoney.com/"]
    start_urls = (
            'http://quote.eastmoney.com/sz300166.html', # 东方国信
            'http://quote.eastmoney.com/sz000001.html', # 平安银行
    )

    rules = [
                Rule(SgmlLinkExtractor(allow=('sh\w+.html$', )),
                    callback='parse_stock', process_request='add_cookie'),
                Rule(SgmlLinkExtractor(allow=('list.html', )), follow=True, process_request='add_cookie')
            ]

    def parse(self, response):
        # fill the field in StockInfoItem
        # item = StockInfoItem() 

        #items = \
        #        response.selector.xpath('''//*[@id="BIZ_MS_dxph"]/tbody/tr[*]/td[position()=2
        #        or position() = 3 or position()=4]''').extract()

        # collect `item_urls`
        pass

    def add_cookie(self, request):
        request.replace(cookies={'name':'COOKIE_NAME', 'value':'VALUE',
            'domain':'.eastmoney.com', 'path':'/'})
        return request
