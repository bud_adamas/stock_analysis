# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

from scrapy.item import Item, Field

class StockInfoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    stock_id        = Field()   # 股票代码
    stock_name      = Field()   # 名称
    stock_price     = Field()   # 股价
