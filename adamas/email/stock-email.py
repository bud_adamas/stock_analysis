#!/usr/bin/env python3
# -*- encoding: UTF-8 -*-

from smtplib import SMTP_SSL
from imaplib import IMAP4_SSL
from email.mime.text import MIMEText
from email.header import decode_header
from email.message import Message
from email import message_from_bytes
from datetime import datetime,timedelta,timezone
from getpass import getpass

import pdb

user = '286728050@qq.com'
#user = '3353106798@qq.com'
service_domain = 'qq.com'
address = user
commander = '3353106798@qq.com'

def decoded_header(self, key):
    t = decode_header(self[key])
    l = [item[0] if isinstance(item[0], str) \
            else item[0].decode('ascii' if item[1] == None else item[1]) \
            for item in t]
    return ''.join(l)
Message.decoded_header = decoded_header

def fetch_as_msg(self, num, msg_filter):
    typ, data = self.fetch(num, msg_filter)
    msg = message_from_bytes(data[0][1])
    return msg
IMAP4_SSL.fetch_as_msg = fetch_as_msg

def init_then_send(self):
    self['From'] = address
    self['To'] = commander
    sender = SMTP_SSL('smtp.%s' % service_domain)
    sender.login(user, password)
    sender.sendmail(address, [commander], self.as_string())
    sender.quit()
MIMEText.init_then_send = init_then_send

#password = getpass('Password(%s): ' % user)
password = 'zhu26wen23'
print('----------------------------------------------------')
while True:
    try:
        server = IMAP4_SSL('imap.%s' % service_domain)

        try:
            # try to login into the mail server
            server.login(user, password)
        except Exception as e:
            print(e.args[0].decode('gbk'))
            exit()

        # the directories name is encoded in UTF-7
        r, msg_count = server.select('INBOX')
        mail_dirs = server.list()[1]
        for mail_dir in mail_dirs:
            dir_name = mail_dir.split(b'"')[3]
            if dir_name.find(b'&') != -1 and \
                    dir_name.find(b'-') != -1:
                        dir_name = dir_name.replace(b'&', b'+')
                        try:
                            dir_name = dir_name.decode('UTF-7')
                        except UnicodeDecodeError:
                            pass
                        '''
                        >>> www='汇编语言'
                        >>> www.encode('UTF-7')
                        b'+bEd/FovtigA-'
                        从服务器获取的实际内容是 b'+bEd,FovtigA-'
                        '''
            #print(dir_name)
        #pdb.set_trace()
        r, data = server.search(None, '(NEW)')
        tasks = []
        for num in data[0].decode().split():
            msg = server.fetch_as_msg(num, '(BODY[HEADER.FIELDS (SUBJECT FROM DATE CONTENT-TYPE CONTENT-TRANSFER-ENCODING)])')
            subject = msg.decoded_header('Subject')
            sender = msg.decoded_header('From')
            date = msg['Date']
            ctype = msg.get_content_type()
            print('(%s) %s : %s {%s}' % (num, sender, subject, ctype))
            if ctype.find('text/plain') >= 0 and within_30m(date):
                    #msg = server.fetch_as_msg(num, '(RFC822)')
                    msg.set_payload(server.fetch(num, '(BODY[TEXT])')[1][0][1].decode('ascii'))
                    # first decode with Content-Transfer-Encoding such as base64
                    cmd_bytes = msg.get_payload(decode=True)
                    # then decode the byte array to string with the charset in Content-Type
                    charset = msg.get_param('charset')
                    tasks.append((date, cmd_bytes.decode('ascii' if charset == None else charset)))
            else:
                    print('\t(Content-Type or Date invalid)')
        #server.expunge()

        # send a message
        msg_text = str(datetime.now())
        msg = MIMEText(msg_text, 'html')
        msg['Subject'] = 'From Adamas'
        msg.init_then_send()
        print("A mail was sent")

        server.close()  # Deleted files will now be purged
        server.logout()
    except Exception:
        # most probablly a network failure
        traceback.print_exc()
    finally:
        print('----------------------------------------------------')
print('Shutting down this agent...')
