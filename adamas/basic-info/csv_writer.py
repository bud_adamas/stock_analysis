#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

import csv

# ������
import pdb

def write_csv(filename, content = None, header = None): 
    file = open(filename, "a")
    file.write('\xEF\xBB\xBF')
    writer = csv.writer(file, delimiter=',')
    if header:
        writer.writerow(header)
    if content:
        writer.writerow(content)
