#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

from urllib2 import urlopen
import urlparse
import bs4
import re

from csv_writer import *

# 调试用
import pdb
# pdb.set_trace()
 
# 同花顺信息
# 包含如下页面：

# 信息概览
# http://stockpage.10jqka.com.cn/600809/

BASE_URL = "http://stockpage.10jqka.com.cn/"

keys=['上市日期',
      '主营业务',
      '总股本',
      '流通股',
      '营业收入增长率',
      '营业收入',
      '净利润',
      '净利润增长率',
      '每股收益',
      '每股净资产',
      '净资产收益率',
      '每股现金流',
      '每股公积金',
      '每股未分配利润']

# 包含如下栏目，貌似用处不大：
# 行情 走势 特别提示 最新消息 财务指标 龙虎榜 大宗交易 融资融券 股吧

def get_intro_info(stock_code, csv_file):
    url = BASE_URL + str(stock_code)
    infourl = urlopen(url)
    if infourl.url == BASE_URL:
        # 出错了
        print "代码 " + str(stock_code) + " *出错*"
        return None
    else:
        print "代码 " + str(stock_code) + " 正常"

    html = infourl.read()
    soup = bs4.BeautifulSoup(html, "html5lib")

    # 公司基本信息
    info = {}
    page_info = soup.select('body > div.m_content > div:nth-of-type(6) > div:nth-of-type(2) > dl > dd:nth-of-type_(2)')
    if len(page_info) != 15:
        raise Exception('"信息概览"页面中的已经发生变化，请联系\
 Adamas(adamas@tom.com) 更新代码')

    # 第二行不需要，因为需要的信息在第三行
    del page_info[1]

    infos = [ re.sub('　+', '',     # 去除行首的中文空格
              re.sub('<.*?>', '',   # 去除 <> HTML 标记
                str(info))) for info in page_info ]

    # 预处理
    #pdb.set_trace()
    result = []
    for info in infos:
        # 如果包含’%‘，转换成小数,保留小数点后四位
        if re.search("%$", str(info)) != None:
            info0 = float(re.sub('%', '', str(info)))/100
            info = float('%0.4f'%info0)
        elif re.search("亿", str(info)) != None:
            info = float(re.sub('亿.*', '', str(info)))
        # 写入另一个列表中
        result.append(info)

    # 输出，检查用
    #for key in result:
    #    print key

    # 插入股票代码
    result.insert(0, stock_code)

    # 序列化到文件中
    write_csv(csv_file, result)

# 序列化，写出到
# * csv 文件中，可以导入到数据库中，或 R 中；
# * 数据库中，直接进行分析。
# def serialization(info)




























# 资金流向
# http://stockpage.10jqka.com.cn/600809/funds/

# 公司资料
# http://stockpage.10jqka.com.cn/600809/company/

# 新闻公告
# http://stockpage.10jqka.com.cn/600809/news/

# 财务分析
# http://stockpage.10jqka.com.cn/600809/finance/

# 经营分析
# http://stockpage.10jqka.com.cn/600809/operate/

# 股东股本
# http://stockpage.10jqka.com.cn/600809/holder/

# 主力持仓
# http://stockpage.10jqka.com.cn/600809/position/

# 公司大事
# http://stockpage.10jqka.com.cn/600809/event/

# 分红融资
# http://stockpage.10jqka.com.cn/600809/bonus/

# 价值分析
# http://stockpage.10jqka.com.cn/600809/worth/

# 行业地位
# http://stockpage.10jqka.com.cn/600809/field/
