#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

import thread

from info import *

# 沪市 A 股以 600、601、603 打头
ids1 = range(600000, 600999)
ids2 = range(601000, 601999)
ids3 = range(603000, 603999)

# 深市 A 股以 000 打头
ids4 = range(0, 999)

ids = ids1 + ids2 + ids3 + ids4

if __name__ == '__main__':
    # 不写标题头
    #write_csv('file.csv', header = [ '股票代码' ] + keys)

    for id in ids:
    #for id in [600425]:
        info = get_intro_info("%06d" % id, "file.csv")
        # 603592 出错
