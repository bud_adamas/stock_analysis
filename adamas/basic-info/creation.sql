-- 目前使用 Sqlite3 的语法

-- 创建表
--  .read creation.sql
-- 设置 csv 分隔符
--  .separator ","
-- 导入 csv
--  .import file.csv table_name

-- create table for basic information.
-- 基本信息
-- DROP TABLE IF EXISTS basic_info;
CREATE TABLE basic_info (
    id                          INT,    -- 股票代码
    date                        TEXT,   -- 上市日期
    business                    TEXT,   -- 主营业务
    total_capital               INT,    -- 总股本（单位：亿）
    circulating_capital         INT,    -- 流通股（单位：亿）
    income_increasing_rating    DOUBLE, -- 营业收入增长率
    income                      INT,    -- 营业收入（单位：亿）
    profile                     INT,    -- 净利润（单位：亿）
    profile_increasing_rating   DOUBLE, -- 净利润增长率
    income_pre_share            DOUBLE, -- 每股收益（单位：元）
    asset_per_share             DOUBLE, -- 每股净资产（单位：元）
    asset_return_rating         DOUBLE, -- 净资产收益率
    cash_flow_per_share         DOUBLE, -- 每股现金流（单位：元）
    fund_per_share              DOUBLE, -- 每股公积金（单位：元）
    unpaid_profile_per_share    DOUBLE  -- 每股未分配利润（单位：元）
    );
