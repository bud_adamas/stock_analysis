#!/usr/bin/env phantomjs

var page = require('webpage').create();

page.open('https://licai.lianjia.com/licai', function(status) {
        if (status !== 'success') {
            console.log('Unable to access network');
        } else {
            var ua = page.evaluate(function() {
                // fetch every line
                trs = document.querySelector("body > div.main.clearfix > div.newShare.bid_list_dv > table > tbody").children;
                for( var i=1, j=trs.length; i < j ; i++) {
                    // sort 1: 预约
                    // sort 2: 正在出售？
                    // sort 3: 售罄
                    if (trs[i].querySelector("td.bid > a").text == "已售罄" ) {
                        // 已售罄
                    } else if (trs[i].querySelector("td.bid > a").text == "" ) {
                        // 预约购买
                    } else {
                        // 如果有“还剩00:00:xx”，立即返回
                        // 或”购买“
                        return true;
                    }
                }
                // 没有找到“正在出售”的
                return false;
            });
            console.log(ua);
            if (ua == true) {
                console.log("正在出售");
            } else if (ua == false) {
                console.log("已售罄");
            }
        }
        phantom.exit();
        });
