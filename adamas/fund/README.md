Fetch the info of the funds in Lianjia, and report it if any fund is available.

抓取链家地产基金销售情况，并在可购时进行邮件通知.

目前实现功能：
    * 抓取网页 JavaScript 产生的信息
    * 邮件进行通知

TODO:
    * 目前的两个功能分别实现，需要将其整合为一个，在发现可购时发送邮件
