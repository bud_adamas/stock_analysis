# -*- coding: utf-8 -*-

from scrapy.spiders import Spider 
import bs4
import re
import sys
import pdb

sys.path.append('../quote-info/')
import quote_rt

  
class DmozSpider(Spider): 
    name = "dmoz"
    allowed_domains = ["q.stock.sohu.com"] 

    start_urls = [ 
        "http://q.stock.sohu.com/cn/dxph_jzc.shtml", 
    ] 
  
    def parse(self, response): 
        #filename = response.url.split("/")[-2] 
        #open(filename, 'wb').write(response.body)

        # with html tags
        # column2 : id
        # column3 : name
        # column4 : capital
        items = \
        response.selector.xpath('''//*[@id="BIZ_MS_dxph"]/tbody/tr[*]/td[position()=2
                or position() = 3 or position()=4]''').extract()

        # group every three items into a group
        stocks_orig = [ tuple(items[i:i+3]) for i in range(0, len(items), 3)]

        # process every stock
        stocks = []
        for stock in stocks_orig:
            id = re.sub('<.*?>', '', stock[0])
            name = re.sub('<.*?>', '', stock[1])
            capital = re.sub('[\t\r\n]+', '', re.sub('<.*?>', '', stock[2]))

            stocks.append(tuple([id, name, capital]))

            capital_num = float(capital)
            try:
                price_num = quote_rt.get_quote(id)['price']
            except Exception, e:
                print e.message
                continue

            #print name + ": " + str(price_num/capital_num)
            try:
                #print price_num/capital_num
                print str(price_num/capital_num) + " : " + name.encode('UTF-8')
            except UnicodeEncodeError, e:
                print e.message
                #pdb.set_trace()
                print "A error happend when transfering to unicode"
                continue
