        -- set up the table quote_rt
CREATE TABLE quote_rt(
    "arrow"     CHAR(4),                -- 箭头，\u2193 下跌
    "ask1"      FLOAT,                  -- 卖一价
    "ask2"      FLOAT,                  -- 卖二价
    "ask3"      FLOAT,                  -- 卖三价
    "ask4"      FLOAT,                  -- 卖四价
    "ask5"      FLOAT,                  -- 卖五价
    "askvol1"   INTEGER,                -- 卖一量(手)
    "askvol2"   INTEGER,                -- 卖二量
    "askvol3"   INTEGER,                -- 卖三量
    "askvol4"   INTEGER,                -- 卖四量
    "askvol5"   INTEGER,                -- 卖五量
    "bid1"      FLOAT,                  -- 买一价
    "bid2"      FLOAT,                  -- 买二价
    "bid3"      FLOAT,                  -- 买三价
    "bid4"      FLOAT,                  -- 买四价
    "bid5"      FLOAT,                  -- 买五价
    "bidvol1"   INTEGER,                -- 买一量(手)
    "bidvol2"   INTEGER,                -- 买二量
    "bidvol3"   INTEGER,                -- 买三量
    "bidvol4"   INTEGER,                -- 买四量
    "bidvol5"   INTEGER,                -- 买五量
    "code"      CHAR(10),               -- 网易内部代码，添加了 0 或 1
    "high"      FLOAT,                  -- 最高价
    "low"       FLOAT,                  -- 最低价
    "name"      CHAR(10),               -- 股票名称(汉字、字母)
    "open"      FLOAT,                  -- 开盘价
    "percent"   FLOAT,                  -- 涨跌百分比
    "price"     FLOAT,                  -- 报价
    "status"    INTEGER,                -- 状态，0 表示成功
    "symbol"    CHAR(10),               -- 通用的股票代码
    "time"      TIMESTAMP,              -- 具体时间，精确到秒
    "turnover"  FLOAT,                  -- 总手金额
    "type"      CHAR(4),                -- 类型，沪市或深市
    "update"    TIMESTAMP,              -- 更新时间
    "updown"    FLOAT,                  -- 涨跌数
    "volume"    INTEGER,                -- 总手
    "yestclose" FLOAT,                  -- 昨日收盘价
    PRIMARY KEY ("symbol", "update")    -- 主键，防止停盘后重复插入数据
                                        -- NOTICE：假设每秒钟更新一次(不靠谱)
);
