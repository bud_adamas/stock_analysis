#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

from urllib2 import urlopen
import simplejson as json
import re

import pdb

# URL looks like
# http://api.money.126.net/data/feed/1000001,money.api

url_prefix = "http://api.money.126.net/data/feed/"
url_postfix = "money.api"

# stock_codes: input parameter, stock_code codes
# infos: a list of maps, which include code, name, price and so on
def get_quote(stock_codes, infos):

    # 按照网易财经特殊规则修订股票代码：
    # According to the rule of netease:
    #   the prefix fo 000xxx is 1
    #   the prefix fo 002xxx is 1
    #   the prefix fo 200xxx is 1
    #   the prefix fo 300xxx is 1
    #   the prefix fo 600xxx is 0
    #
    stock_codes_x = ""
    for stock_id in stock_codes:
        str_stock_id = str(stock_id)
        if re.search("^6.*", str_stock_id):
            str_stock_idx = "0" + str_stock_id
        elif re.search("^000.*", str_stock_id):
            str_stock_idx = "1" + str_stock_id
        elif re.search("^300.*", str_stock_id):
            str_stock_idx = "1" + str_stock_id
        elif re.search("^002.*", str_stock_id):
            str_stock_idx = "1" + str_stock_id
        elif re.search("^200.*", str_stock_id):
            str_stock_idx = "1" + str_stock_id
        else:
            raise Exception("不能将股票代码 " + str(stock_id) + \
                    " 转换为网易财经内部代码")

        stock_codes_x += str_stock_idx + ","

    url = url_prefix + stock_codes_x + url_postfix
    page_content = urlopen(url).read()

    # 取出 _ntes_quote_callback 中的内容
    str_contents = re.search('_ntes_quote_callback\((.*)\)', page_content).group(1)

    # 解析 json 格式
    json_content = json.loads(str_contents)

    # only one key
    keys = json_content.keys()
    for key in keys:
        infos.append(json_content[key])
