#!/usr/bin/env python

# -*- encoding: UTF-8 -*-
from get_quote import get_quote
from daemon import Daemon

import re
import sqlite3
import time
import os
import logging
import sys

__author__ = 'adamas'

# file used by after daemonize, so absolute path
database_file = os.getcwd() + '/quote_repository.sqlite3'
portfolio_file = os.getcwd() + '/portfolio'

# before daemonize, so relative path
log_file = "quote_daemon.log"
pid_file = '/tmp/quote_daemon.pid'

class QuoteServer(Daemon):


    def _run(self):
        logger.error("start run xxx")

        # read the portfolio_file
        # TODO(adamas): add exception handling
        logger.error("file opening")
        try:
            with open(portfolio_file) as f:
                original_code = f.readlines()
        except IOError as e:
            logger.error('Error happened ')
            logger.error("Error message: " + str(e))
            exit(2)

        logger.error("file opened")

        # connect to a SQLite3 database
        # TODO(adamas): add exception handling
        # e.g. check the existance of the table
        dbconn = sqlite3.connect(database_file)
        dbcursor = dbconn.cursor()

        # remove the comments, and the return charcter,
        # and then add them to another list.
        stock_codes = []
        for stock_code in original_code:
            stock_codes.append(re.sub('[ \t]*#.*\n', '', stock_code))

        hash_code = 0
        while True:
            # result info
            infos = []
            get_quote(stock_codes, infos)

            logger.info("hash_code is " + str(hash_code))
            if hash_code == hash(str(infos)):
                logger.info("Got a duplicate one, sleep and go on")
                time.sleep(1)
                continue
            else:
                hash_code = hash(str(infos))

            stock_tuples = []
            for info in infos:
                try:
                    dbcursor.execute("""INSERT INTO quote_rt VALUES (?,?,?,?,?,?,?,?,?,?,
                                                                 ?,?,?,?,?,?,?,?,?,?,
                                                                 ?,?,?,?,?,?,?,?,?,?,
                                                                 ?,?,?,?,?,?,?)""",
                                        [info["arrow"],
                                        info["ask1"],
                                        info["ask2"],
                                        info["ask3"],
                                        info["ask4"],
                                        info["ask5"],
                                        info["askvol1"],
                                        info["askvol2"],
                                        info["askvol3"],
                                        info["askvol4"],
                                        info["askvol5"],
                                        info["bid1"],
                                        info["bid2"],
                                        info["bid3"],
                                        info["bid4"],
                                        info["bid5"],
                                        info["bidvol1"],
                                        info["bidvol2"],
                                        info["bidvol3"],
                                        info["bidvol4"],
                                        info["bidvol5"],
                                        info["code"],
                                        info["high"],
                                        info["low"],
                                        info["name"],
                                        info["open"],
                                        info["percent"],
                                        info["price"],
                                        info["status"],
                                        info["symbol"],
                                        info["time"],
                                        info["turnover"],
                                        info["type"],
                                        info["update"],
                                        info["updown"],
                                        info["volume"],
                                        info["yestclose"]])
                except sqlite3.IntegrityError:
                    # if a duplicate one is found, sleep for a second.
                    logger.info("duplicate for " + str(info['symbol']))
                except Exception as e:
                    # something unexpected happened.
                    logger.info("Some unexpected error happened")
                    logger.info("Error Message: " + str(e))

                dbconn.commit()

if __name__ == '__main__':

    # logger
    logger = logging.getLogger()

    formatter = logging.Formatter('%(name)-12s %(asctime)s %(levelname)-8s %(message)s', '%a, %d %b %Y %H:%M:%S',)
    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)

    logger.setLevel(logging.DEBUG)

    # deamon control
    quote_daemon = QuoteServer(pid_file)

    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            quote_daemon.start()
        elif 'stop' == sys.argv[1]:
            quote_daemon.stop()
        elif 'restart' == sys.argv[1]:
            quote_daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
