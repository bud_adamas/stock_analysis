抓取实时(realtime)的报价信息。


使用说明:


TODO lists:
    * 发送、接收邮件，模拟交易
    * 后台连续查询价格，并存入数据库中
        已经可以存入 SQLite3 数据库
        TODO(adamas): 诸条处理主键重复，使得粒度更细
        TODO(adamas): deamonize, 脱离前台终端, 输出日志
        TODO(adamas): 存入 PostgreSQL 数据库中
        TODO(adamas): 计算走势，进行通知
        TODO(adamas): 走势重放系统(playback)
    * 从数据库中取出数据，绘图
        TODO: 绘图
    * 从数据库中的数据中计算各种指标，并绘图
