#!/usr/bin/env python

# -*- encoding: UTF-8 -*-

import re
import string

from get_quote import get_quote

if __name__ == '__main__':
    # read the portfolio
    with open('portfolio') as f:
            original_code = f.readlines()

    # remove the comments, and the return charcter,
    # and then add them to another list.
    stock_codes = []
    for stock_code in original_code:
        stock_codes.append(re.sub('[ \t]*#.*\n', '', stock_code))

    # result info
    infos = []

    get_quote(stock_codes, infos)

    for info in infos:
        # remove all the blanks in *name* field
        info['name'] = string.replace(info['name'], ' ', '')

        # output name, price, and percent
        # align them
        line =  string.ljust(info['name'], 7-len(info['name'])+5) + \
                string.ljust(str(info['price']), 6) + \
                "%+.2f" % (float(info['percent'])*100) + "%"      

        print(line.encode('UTF-8'))
