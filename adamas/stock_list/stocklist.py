# -*- coding: utf-8 -*-

from scrapy.spiders import Spider 
import bs4
import re
import sys
import pdb

sys.path.append('../quote-info/')
import quote_rt

  
class DmozSpider(Spider): 
    name = "all_stocks"
    allowed_domains = ["quote.eastmoney.com"] 

    start_urls = [ 
            "http://quote.eastmoney.com/stocklist.html", 
    ] 
  
    def parse(self, response): 
        sh_stocks = \
        response.selector.xpath('//*[@id="quotesearch"]/ul[1]/li[*]/a').extract()

        sz_stocks = \
        response.selector.xpath('//*[@id="quotesearch"]/ul[2]/li[*]/a').extract()

        all_stocks = sh_stocks + sz_stocks

        for stock in all_stocks:
            stock_fields = re.sub('<.*?>', '', stock)
            groups = re.match('(.*?)\((.*)\)', stock_fields)
            stock_name = groups.group(1).encode('UTF-8')
            stock_id = groups.group(2).encode('UTF-8')

            print stock_name + " : " + stock_id
