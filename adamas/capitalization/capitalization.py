#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

from urllib2 import urlopen
import urlparse
import bs4
import csv
import sys
 
# capitalization， 总股本

# 搜狐证券, 只能取前 60 行
BASE_URL = "http://q.stock.sohu.com/cn/dxph_gb.shtml"
capitalization_header = ['排行','股票代码','股票名称','总股本(万股)']

def get_capitalizations(baseurl):
    html = urlopen(baseurl).read()
    soup = bs4.BeautifulSoup(html, "html5lib")
    capitalizations = [ dd for dd in soup.select('#BIZ_MS_dxph > tbody > tr')]
    result = []
    for capitalization in capitalizations:
        record = []
        link = ''
        query = []
        for item in capitalization.contents:
            if type(item) is bs4.element.Tag:
                if item.string != None:
                    record.append(item.string.strip())
        result.append(record)
    return result
     
def write_csv(filename, content, header = None): 
    file = open(filename, "wb")
    file.write('\xEF\xBB\xBF')
    writer = csv.writer(file, delimiter=',')
    if header:
        writer.writerow(header)
    for row in content:
        encoderow = [dd.encode('utf8') for dd in row]
        writer.writerow(encoderow)

if len(sys.argv) == 1:
    print("错误：需要给出保存结果的文件名")
    sys.exit(0)

result = []
for url in [ BASE_URL ]:
    result = result +  get_capitalizations(url)

# 总股本
write_csv(sys.argv[1]+'.csv', result, capitalization_header)
